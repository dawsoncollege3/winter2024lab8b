// Start//

public enum Tile {

    BLANK("_"),
    HIDDEN_WALL("_"),
    W("WALL"),
    C("CASTLE");

    // Fields//

    private final String name;

    // Constructor//

    private Tile(String name) {
        this.name = name;
    }

    // Get Method//

    public String getName() {
        return this.name;
    }
}

// End//
