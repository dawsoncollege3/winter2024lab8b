// Start//

// Necessary Commands//

import java.util.Scanner;

// Main Method//

public class BoardGameApp {
    public static void main(String[] args) {

        // Declaring A Board Obkect//

        Board gamePlay = new Board();

        // Connecting To The Keyboard//

        Scanner keyboard = new Scanner(System.in);

        // Collecting User Inputs//

        System.out.println("Enter Your Row Number");
        int userRow = keyboard.nextInt();

        System.out.println("Enter Your Column Number");
        int userColumn = keyboard.nextInt();

        // Output Before placeToken Method//

        System.out.println(gamePlay);

        // Calling The placeToken Method//

        int callingToken = gamePlay.placeToken(userRow, userColumn);

        // Output Before placeToken Method//

        System.out.println(gamePlay);


       
    }
}