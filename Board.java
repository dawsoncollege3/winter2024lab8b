// Necessary Commands//

import java.util.Random;

// Start//

public class Board {

    // Fields//

    private Tile[][] grid;

    // Constructor//

    public Board() {
        this.grid = new Tile[5][5];
        for (int i = 0; i < this.grid.length; i++) {
            for (int j = 0; j < this.grid[i].length; j++) {
                grid[i][j] = Tile.BLANK;
            }
        }

        // Random Wall Positions//

        for (int z = 0; z < 5; z++) {
            Random rng = new Random();
            int randomX = rng.nextInt(grid.length);
            this.grid[z][randomX] = Tile.HIDDEN_WALL;
        }
    }

    // Implementing toString Method//

    public String toString() {
        String showBoard = "";

        for (int i = 0; i < this.grid.length; i++) {
            for (int j = 0; j < this.grid[i].length; j++) {
                showBoard += " | " + this.grid[i][j].getName() + " | ";
            }
            showBoard += "\n";
        }
        return showBoard;
    }

    // Implementing PlayerRoles//

    public int placeToken(int y, int x) {

        // Initate an Int//

        int playerPlay = 0;

        // Check For Board Range (-2)//

        if (!(y >= 0 && y <=5 ) || !(x >= 0 && x <= 5)) {
            playerPlay = -2;
        }

        // Check For W("WALL") && C("CASTLE")//

        if ((this.grid[y][x] == Tile.W) || (this.grid[y][x] == Tile.C)) {
            playerPlay = -1;
        }

        // Check For HIDDEN_WALL("_")//

        if (this.grid[y][x] == Tile.HIDDEN_WALL) {
            playerPlay = 1;
        }

        // Checl If Castle Can Be Placed//

        if (this.grid[y][x] == Tile.BLANK) {
            this.grid[y][x] = Tile.C;
            playerPlay = 0;
        }

        return playerPlay;
    }
}

// End//